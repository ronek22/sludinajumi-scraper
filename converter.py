from glob import glob
import pandas as pd


def read_result_file():
    files = glob("results*")
    files = sorted(files, key=lambda name: name.split('_')[1][:-4])
    result_path = files[0] if files else None
    if result_path is None:
        raise FileNotFoundError("Can't find any result file, maybe need to run spider")

    return pd.read_json(result_path, encoding='utf-8')


if __name__ == '__main__':
    all_cars = read_result_file()

    companies = all_cars.groupby(['phone', 'place'], as_index=False).agg({
        'address': 'first',
        'company': 'first',
        'url': 'first',
        'price': ['sum', 'count']
    })

    companies.to_csv('companies.csv')
    companies.to_excel('companies.xlsx')
