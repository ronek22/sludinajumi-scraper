# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Car(scrapy.Item):
    name = scrapy.Field()
    price = scrapy.Field()
    phone = scrapy.Field()
    place = scrapy.Field()
    company = scrapy.Field()
    address = scrapy.Field()
    url = scrapy.Field()

