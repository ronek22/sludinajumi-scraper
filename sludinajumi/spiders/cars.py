# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime

# RUN WITH 
# scrapy crawl cars -L WARNING
from sludinajumi.items import Car


class CarsSpider(scrapy.Spider):
    name = 'cars'
    allowed_domains = ['ss.com']
    start_urls = [l.strip() for l in open('./urls.txt').readlines()]
    # start_urls = ['https://www.ss.com/lv/transport/cars/alfa-romeo/']
    custom_settings = {
        'FEED_FORMAT': 'json',
        'FEED_URI': 'results_ ' + datetime.now().strftime("%Y%m%d-%H%M%S") + '.json',
        'FEED_EXPORT_ENCODING': 'utf-8'
    }

    def parse(self, response):
        print(response.url)
        cars_links = response.xpath("//td[@class='msga2']/a/@href").extract()

        for car in cars_links:
            yield scrapy.Request(
                response.urljoin(car),
                callback=self.parse_car
            )
        
        next_page = response.xpath("//div[@class='td2']//a/@href")
        next_page = next_page[-1].extract() if next_page else None

        if next_page and 'html' in next_page:
            yield scrapy.Request(
                response.urljoin(next_page),
                callback=self.parse
            )

    def parse_car(self, response):
        name = response.xpath("//td[@id='tdo_31']//text()").extract()
        name = name[0] if name else None

        phone = "".join(response.xpath("//span[@id='phone_td_1']//text()").extract()[:2])
        place = response.xpath("//td[text()='Vieta:']/following-sibling::td[1]/text()").extract()[0]

        price = response.xpath("//span[@class='ads_price']//text()").extract()
        if price:
            price = self.str2int(price[0])
        else:
            return

        company = response.xpath("//td[text()='Uzņēmums:']/following-sibling::td[1]/text()").extract()
        company = company[0] if company else None

        address = response.xpath("//td[text()='Adrese:']/following-sibling::td[1]/a/text()").extract()
        address = address[0] if address else None

        yield Car(
            name=name, price=price, phone=phone, place=place,
            company=company, address=address, url=response.url
        )


    def str2int(self, number: str) -> int:
        if '.' in number:
            number = number.split('.')[0]
            number = "".join(number.split())
        else:
            number = "".join(number[:-1].split())
        return int(number )



